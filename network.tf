resource "aws_vpc" "vpc_Docker" {
  tags = {
    Name="VPC_Docker"
  }
  cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "igw_Docker" {
  vpc_id = aws_vpc.vpc_Docker.id
  tags = {
    Name="IGW_Docker"
  }
}

resource "aws_subnet" "sbn_pu_Docker" {
  vpc_id = aws_vpc.vpc_Docker.id
  cidr_block = "10.0.1.0/28"
  tags = {
    Name="SBNPU_Docker"
  }
}

resource "aws_subnet" "sbn_pr_Docker" {
  vpc_id = aws_vpc.vpc_Docker.id
  cidr_block = "10.0.2.0/28"
  tags = {
    Name="SBNPR_Docker"
  }
}

resource "aws_route_table" "rt_pu_Docker" {
  vpc_id = aws_vpc.vpc_Docker.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_Docker.id
  }
}

resource "aws_route_table_association" "rta-pu-sbn-WPMIG2" {
  route_table_id = aws_route_table.rt_pu_Docker.id
  subnet_id = aws_subnet.sbn_pu_Docker.id
}

resource "aws_route_table" "rt_pr_Docker" {
  vpc_id = aws_vpc.vpc_Docker.id

}

resource "aws_route_table_association" "rta-pr-sbn_Docker" {
  route_table_id = aws_route_table.rt_pr_Docker.id
  subnet_id = aws_subnet.sbn_pr_Docker.id
}