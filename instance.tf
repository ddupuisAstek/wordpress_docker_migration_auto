locals {
  ssh_user = "ubuntu"
  key_name = "LAB1"
  private_key_path = "~/Downloads/LAB1.pem"
}

resource "aws_instance" "wordpress_Docker" {
  ami                         = "ami-0d527b8c289b4af7f"
  instance_type               = "t3.xlarge"
  key_name                    = local.key_name
  associate_public_ip_address = true
  security_groups             = [aws_security_group.SG-SBNPU_Docker.id]
  subnet_id                   = aws_subnet.sbn_pu_Docker.id
  tags                        = {
    Name = "EC2_Docker"
  }


  provisioner "remote-exec" {
    inline = ["echo 'Wait until SSH is ready'"]


    connection {
      type        = "ssh"
      user        = local.ssh_user
      private_key = file(local.private_key_path)
      host        = aws_instance.wordpress_Docker.public_ip
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i ${aws_instance.wordpress_Docker.public_ip}, --private-key ${local.private_key_path} wordpress.yaml"
  }
}

output "WP_IP" {
  value = aws_instance.wordpress_Docker.public_ip
}
